# Resume App

Simple resume app in rails 4.2.2. with bootstrap frontend and activeadmin backend. 

Can be seen at http://resume.adamlwalker.com

There is a db:seed to get started if you want to fork and modify for your own uses.

TODO:
Tests
remove hardcoded personal elements.
Switch to postgresql
Create multiuser version with multiple selectable themes
Add printable:boolean and create hidden-print switch in views

